package network;

import helpers.ConsoleHelper;

import java.io.*;
import java.util.*;

import util.ServerRequestStatus;
import abstracts.*;

/**
 * This class manages all server requests in different states, as well as handles responses from the server.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ServerRequestManager extends Manager<UUID>
{
	private final String NAME = this.getClass().getSimpleName();

	private Queue<ServerRequest> serverRequestQueue;

	private ServerRequestExecutor serverRequestExecutor;

	private ServerListener serverListener;

	/**
	 * Instantiates a new server request manager.
	 */
	public ServerRequestManager()
	{
		serverRequestQueue = new LinkedList<>();
	}

	/**
	 * Finalize the ServerRequestManager. <br>
	 * The purpose of this method is to allow the constructor to return before passing {@code this} to other parts of the system. <br>
	 * Because the object is not fully constructed <i>before</i> the constructor returns and this can cause problems.
	 *
	 * @param outputStream
	 *            the output stream
	 * @param inputStream
	 *            the input stream
	 */
	public void finalize(ObjectOutputStream outputStream, ObjectInputStream inputStream)
	{
		serverRequestExecutor = new ServerRequestExecutor(this, outputStream);
		serverListener = new ServerListener(this, inputStream);

		serverRequestExecutor.start();
		serverListener.start();

		ConsoleHelper.announce(NAME, "Ready.");
	}

	/**
	 * Adds a request to the queue.
	 *
	 * @param serverRequest
	 *            the server request
	 */
	public void addRequest(ServerRequest serverRequest)
	{
		putEntity(serverRequest);
		serverRequestQueue.add(serverRequest);
	}

	/**
	 * Gets the next pending server request.
	 *
	 * @return the next server request
	 */
	public ServerRequest getNextServerRequest()
	{
		synchronized (serverRequestQueue)
		{
			if (serverRequestQueue.peek() != null)
			{
				return serverRequestQueue.poll();
			}

			return null;
		}
	}

	/**
	 * Gets a request from the managed collection of requests.
	 *
	 * @param identifier
	 *            the identifier
	 * @return the request
	 */
	public ServerRequest getRequest(UUID identifier)
	{
		return (ServerRequest) getEntity(identifier);
	}

	/**
	 * Complete a request. This is called whenever a response is received from the server.
	 *
	 * @param networkPackage
	 *            the network package
	 */
	public void completeRequest(NetworkPackage networkPackage)
	{
		ServerRequest sr = getRequest(networkPackage.getPackageId());

		if (sr != null)
		{
			sr.setResultPackage(networkPackage);
			sr.setServerRequestStatus(ServerRequestStatus.COMPLETE);
			ConsoleHelper.announce(NAME, "Processed reply from server" + (networkPackage.failed() ? " (failed request)." : "."));
		}
		else
		{
			ConsoleHelper.announce(NAME, "Got unknown reply from server (reply discarded).");
		}
	}

	/**
	 * Gets the server request status of a request.
	 *
	 * @param identifier
	 *            the identifier
	 * @return the server request status
	 */
	public ServerRequestStatus getServerRequestStatus(UUID identifier)
	{
		return getRequest(identifier).getServerRequestStatus();
	}

	/**
	 * Wait for complete request.
	 *
	 * @param identifier
	 *            the identifier
	 * @return the server request
	 */
	public ServerRequest waitForCompleteRequest(UUID identifier)
	{
		while (getServerRequestStatus(identifier) != ServerRequestStatus.COMPLETE)
		{
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{
				// e.printStackTrace();
			}
		}

		return getRequest(identifier);
	}

	/**
	 * Shutdown the threads, stopping the asynchronous executions.
	 */
	public void shutdown()
	{
		ConsoleHelper.announce(NAME, "Stopping asynchronous operations.");
		serverRequestExecutor.abort();
		serverListener.abort();
	}

	/**
	 * Gets all pending requests.
	 *
	 * @return all pending requests
	 */
	public ArrayList<UUID> getAllPendingRequests()
	{
		ArrayList<UUID> idList = new ArrayList<UUID>();
		
		for (Manageable<UUID> request : getAllEntities())
		{
			ServerRequest sreq = (ServerRequest) request;

			if (sreq.getServerRequestStatus() != ServerRequestStatus.COMPLETE)
			{
				idList.add(sreq.getIdentifier());
			}
		}

		return idList;
	}

	/**
	 * Gets all requests.
	 *
	 * @return all requests
	 */
	public ArrayList<ServerRequest> getAllRequests()
	{
		ArrayList<ServerRequest> requestList = new ArrayList<ServerRequest>();

		ArrayList<Manageable<UUID>> allRequests = getAllEntities();
		for (Manageable<UUID> request : allRequests)
		{
			ServerRequest sreq = (ServerRequest) request;
			requestList.add(sreq);
		}

		return requestList;
	}
}
