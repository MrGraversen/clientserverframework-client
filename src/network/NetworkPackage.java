package network;
import java.io.Serializable;
import java.util.UUID;

import util.RequestType;
import abstracts.DynamicPropertyEntity;
import exceptions.OwnzoneException;

/**
 * This class is sent over the network connection and is recognized by both the server and the client parts of the framework. It's a generic class for transferring data and requesting tasks.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class NetworkPackage extends DynamicPropertyEntity implements Serializable
{
	private UUID packageId;
	
	private UUID clientId;

	private int requestType;

	private String taskIdentifier;

	private OwnzoneException exception;

	/**
	 * Instantiates a new network package.
	 */
	public NetworkPackage()
	{
	}

	/**
	 * Gets the request type.
	 * @see RequestType
	 *
	 * @return the request type
	 */
	public int getRequestType()
	{
		return requestType;
	}

	/**
	 * Gets the task identifier.
	 *
	 * @return the task identifier
	 */
	public String getTaskIdentifier()
	{
		return taskIdentifier;
	}

	/**
	 * Gets the package id.
	 *
	 * @return the package id
	 */
	public UUID getPackageId()
	{
		return packageId;
	}
	
	/**
	 * Sets the package id.
	 * <br>This id is the same across both server and client because it makes a "transaction" recognizable.
	 *
	 * @param packageId the new package id
	 */
	public void setPackageId(UUID packageId)
	{
		this.packageId = packageId;
	}

	/**
	 * Sets the client id.
	 *
	 * @param clientId the new client id
	 */
	public void setClientId(UUID clientId)
	{
		this.clientId = clientId;
	}

	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public UUID getClientId()
	{
		return clientId;
	}

	/**
	 * Gets the exception (if any).
	 *
	 * @return the exception
	 */
	public OwnzoneException getException()
	{
		return exception;
	}

	/**
	 * Sets the exception, indicating that this is a failed package.
	 *
	 * @param exception the new exception
	 */
	public void setException(OwnzoneException exception)
	{
		this.exception = exception;
	}

	/**
	 * If this package contains an exception, it means something went wrong during the transaction. 
	 *
	 * @return true, if an exception is set
	 */
	public boolean failed()
	{
		return exception != null;
	}

	/**
	 * Sets the task identifier.
	 *
	 * @param taskIdentifier the new task identifier
	 */
	public void setTaskIdentifier(String taskIdentifier)
	{
		this.taskIdentifier = taskIdentifier;
	}

	/**
	 * Sets the request type.
	 *
	 * @param requestType the new request type
	 */
	public void setRequestType(int requestType)
	{
		this.requestType = requestType;
	}
}
