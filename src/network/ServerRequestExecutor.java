package network;
import helpers.ConsoleHelper;

import java.io.*;
import java.util.*;

/**
 * This class handles all communication to the server component.
 * <br>When a server request is submitted, it will be picked up, converted and sent to the server.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ServerRequestExecutor extends Thread
{
	private final String NAME = this.getClass().getSimpleName();

	private ServerRequestManager serverRequestManager;
	
	private ObjectOutputStream outputStream;

	private final static int THROTTLE_VALUE = 10;
	
	private volatile boolean stop;
	
	private long lastReset;

	/**
	 * Instantiates a new server request executor.
	 *
	 * @param serverRequestManager the server request manager
	 * @param outputStream the output stream to the server
	 */
	public ServerRequestExecutor(ServerRequestManager serverRequestManager, ObjectOutputStream outputStream)
	{
		this.lastReset = System.currentTimeMillis();
		this.outputStream = outputStream;
		this.serverRequestManager = serverRequestManager;

		ConsoleHelper.announce(NAME, "Ready.");
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run()
	{
		while (!stop)
		{
			pollScheduledRequests();
			hibernate(THROTTLE_VALUE);
		}

		// NetworkPackage networkPackage = transformServerRequest();
		//
		// try
		// {
		// synchronized (clientConnection.getOutStream()) // TODO: rapport
		// {
		// clientConnection.getOutStream().writeObject(networkPackage);
		// clientConnection.getOutStream().flush();
		//
		// serverRequest.setServerRequestStatus(ServerRequestStatus.SENT);
		//
		// NetworkPackage networkPackageReply = (NetworkPackage) clientConnection.getInStream().readObject();
		//
		// serverRequest.setResultPackage(networkPackageReply);
		// serverRequest.setServerRequestStatus(ServerRequestStatus.COMPLETE);
		// }
		// }
		// catch (IOException e)
		// {
		// serverRequest.setServerRequestStatus(ServerRequestStatus.COMPLETE);
		// serverRequest.setResultPackage(fail(networkPackage, e));
		// e.printStackTrace();
		// }
		// catch (ClassNotFoundException e)
		// {
		// serverRequest.setServerRequestStatus(ServerRequestStatus.COMPLETE);
		// serverRequest.setResultPackage(fail(networkPackage, e));
		// e.printStackTrace();
		// }
	}

	/**
	 * Poll scheduled requests. If any requests are waiting, convert them and send them to the server.
	 */
	private void pollScheduledRequests()
	{
		synchronized (serverRequestManager)
		{
			ServerRequest sr = serverRequestManager.getNextServerRequest();

			if (sr != null)
			{
				NetworkPackage np = transformServerRequest(sr);
				fire(np);
			}
		}
	}

	/**
	 * Sends a {@link NetworkPackage} to the server.
	 *
	 * @param networkPackage the network package
	 */
	private void fire(NetworkPackage networkPackage)
	{
		try
		{
			outputStream.writeObject(networkPackage);
			outputStream.flush();
			
			if (System.currentTimeMillis() - lastReset > 10000)
			{
				lastReset = System.currentTimeMillis();
				outputStream.reset();
				System.out.println("Reset");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	// private NetworkPackage fail(NetworkPackage networkPackage, OwnzoneException e)
	// {
	// networkPackage.setException(e);
	// return networkPackage;
	// }

	/**
	 * Transform a server request to a {@link NetworkPackage} so that it can be sent over the network.
	 *
	 * @param serverRequest the server request
	 * @return the network package
	 */
	private NetworkPackage transformServerRequest(ServerRequest serverRequest)
	{
		NetworkPackage np = new NetworkPackage();

		np.setTaskIdentifier(serverRequest.getTaskIdentifier());
		np.setRequestType(serverRequest.getRequestType());
		np.setClientId(serverRequest.getClientId());
		np.setPackageId(serverRequest.getIdentifier());
		np.copyFromEntity(serverRequest);

		return np;
	}

	/**
	 * Sleep for the specified duration.
	 *
	 * @param duration the duration
	 */
	private void hibernate(int duration)
	{
		try
		{
			Thread.sleep((long) duration);
		}
		catch (InterruptedException e)
		{
			//e.printStackTrace();
		}
	}

	/**
	 * Abort the thread, stopping the asynchronous execution.
	 */
	public void abort()
	{
		stop = true;
		interrupt();
	}
}
