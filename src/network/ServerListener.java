package network;
import helpers.ConsoleHelper;

import java.io.*;
import java.net.SocketException;

/**
 * This class runs asynchronously and listens for packages from the server and delegates the response received to its respective {@link ServerRequest}.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ServerListener extends Thread
{
	private final String NAME = this.getClass().getSimpleName();

	private final static int THROTTLE_VALUE = 10;

	private volatile boolean stop;

	private ObjectInputStream inputStream;

	private ServerRequestManager serverRequestManager;

	/**
	 * Instantiates a new server listener.
	 *
	 * @param inputStream the input stream for the server connection
	 */
	public ServerListener(ServerRequestManager serverRequestManager, ObjectInputStream inputStream)
	{
		this.serverRequestManager = serverRequestManager;
		this.inputStream = inputStream;

		ConsoleHelper.announce(NAME, "Ready.");
	}

	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run()
	{
		while (!stop)
		{
			pollIncomingRequests();
			hibernate(THROTTLE_VALUE);
		}
	}

	/**
	 * Sleep for the specified duration.
	 *
	 * @param duration the duration
	 */
	private void hibernate(int duration)
	{
		try
		{
			Thread.sleep((long) duration);
		}
		catch (InterruptedException e)
		{
			//e.printStackTrace();
		}
	}

	/**
	 * Poll incoming requests. If any valid response is received, connect it to a its appropriate {@link ServerRequest}.
	 */
	private void pollIncomingRequests()
	{
		try
		{
			NetworkPackage networkPackageReply = (NetworkPackage) inputStream.readObject();
			serverRequestManager.completeRequest(networkPackageReply);
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (SocketException | EOFException e)
		{
			ConsoleHelper.announce(NAME, "Server connection aborted, aborting.");
			serverRequestManager.shutdown();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Abort the thread, stopping the asynchronous execution.
	 */
	public void abort()
	{
		stop = true;
		interrupt();
	}
}
