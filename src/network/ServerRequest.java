package network;

import java.io.*;
import java.util.UUID;

import util.*;
import abstracts.*;

/**
 * This class acts as a proxy for {@link NetworkPackage} objects sent to the server. This allows a developer to more easily build requests without interacting with the network package. <br>
 * The class is generally used to request tasks on the server.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ServerRequest extends DynamicPropertyEntity implements Manageable<UUID>
{

	/** The request type. */
	private int requestType;

	/** The task identifier. */
	private String taskIdentifier;

	/** The server request status. */
	private ServerRequestStatus serverRequestStatus;

	/** The network package result. */
	private NetworkPackage networkPackageResult;

	/** The server request id. */
	private UUID serverRequestId;

	/** The client id. */
	private UUID clientId;

	/**
	 * Instantiates a new server request. Use this constructor when <u>not</u> requesting a task.
	 *
	 * @param requestType
	 *            the request type
	 */
	public ServerRequest(int requestType)
	{
		initialize();
		this.requestType = requestType;
	}

	/**
	 * Instantiates a new server request. Use this constructor when requesting a task.
	 *
	 * @param taskIdentifier
	 *            the task identifier
	 */
	public ServerRequest(String taskIdentifier)
	{
		if(taskIdentifier == null || taskIdentifier.isEmpty()) throw new IllegalArgumentException("Task identifier must be something.");
		initialize();
		requestType = RequestType.GENERIC;
		this.taskIdentifier = taskIdentifier;
	}

	/**
	 * Initialize the request.
	 */
	private void initialize()
	{
		serverRequestId = UUID.randomUUID();
		serverRequestStatus = ServerRequestStatus.WAITING;
	}

	/**
	 * Sets the server request status.
	 *
	 * @param serverRequestStatus
	 *            the new server request status
	 */
	public void setServerRequestStatus(ServerRequestStatus serverRequestStatus)
	{
		this.serverRequestStatus = serverRequestStatus;
	}

	/**
	 * Gets the request type.
	 *
	 * @return the request type
	 */
	public int getRequestType()
	{
		return requestType;
	}

	/**
	 * Gets the server request status.
	 *
	 * @return the server request status
	 */
	public ServerRequestStatus getServerRequestStatus()
	{
		return serverRequestStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see abstracts.Manageable#getIdentifier()
	 */
	public UUID getIdentifier()
	{
		return serverRequestId;
	}

	/**
	 * Gets the task identifier.
	 *
	 * @return the task identifier
	 */
	public String getTaskIdentifier()
	{
		return taskIdentifier;
	}

	/**
	 * Sets the result package. Called when the server responds.
	 *
	 * @param networkPackage
	 *            the new result package
	 */
	public void setResultPackage(NetworkPackage networkPackage)
	{
		networkPackageResult = networkPackage;
	}

	/**
	 * Gets the result package. This package is received by the server after exectution is complete.
	 *
	 * @return the result package
	 */
	public NetworkPackage getResultNetworkPackage()
	{
		return networkPackageResult;
	}

	/**
	 * Gets a result data from the returned ServerRequest.
	 *
	 * @return a DynamicProperty representing the result data
	 */
	public DynamicProperty<?> getResultData(String identifier)
	{
		return networkPackageResult.getProperty(identifier);
	}

	/**
	 * Sets the client id.
	 *
	 * @param clientId
	 *            the new client id
	 */
	public void setClientId(UUID clientId)
	{
		this.clientId = clientId;
	}

	/**
	 * Gets the client id.
	 *
	 * @return the client id
	 */
	public UUID getClientId()
	{
		return clientId;
	}
}
