package network;
import helpers.CryptoHelper;

import java.security.PublicKey;

import statics.*;
import util.*;
import exceptions.OwnzoneException;

/**
 * This class can build a few ready-made requests for communication.
 * <br>For example: ConnectRequest and DisconnectRequest
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ServerRequestFactory
{
	private ServerRequestFactory()
	{

	}

	/**
	 * Builds the connect request. This request is used when the client connects to the server.
	 *
	 * @return the server request
	 * @throws OwnzoneException an exception indicating a problem with building the request
	 */
	public static ServerRequest buildConnectRequest() throws OwnzoneException
	{
		ServerRequest serverRequest = new ServerRequest(RequestType.CONNECT);

		String passwordId = "SERVER_PASSWORD";
		serverRequest.putProperty(new DynamicProperty<String>(passwordId, "", Settings.getServerPassword(), true));
		
		String machineId = "MACHINE_ID";
		serverRequest.putProperty(new DynamicProperty<String>(machineId, "", ThisMachine.getCpuId(), false));
		
		String publicKey = "PUBLIC_KEY";
		serverRequest.putProperty(new DynamicProperty<PublicKey>(publicKey, "", CryptoHelper.getPublicEncryptionKey(), false));
		
		return serverRequest;
	}

	/**
	 * Builds the disconnect request.
	 *
	 * @return the server request
	 */
	public static ServerRequest buildDisconnectRequest()
	{
		ServerRequest serverRequest = new ServerRequest(RequestType.DISCONNECT);
		
		return serverRequest;
	}
}
