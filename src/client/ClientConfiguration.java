package client;

import statics.Settings;
import exceptions.OwnzoneException;

/**
 * The purpose of this class is to act as a primer for the {@link ClientConnection} class, providing necessary start-up information.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ClientConfiguration
{
	private String serverAddress;
	
	private int serverPort;
	
	private String serverPassword;
		
	/**
	 * Instantiates a new client configuration.
	 *
	 * @param serverAddress the server address (IPv4 address or host name)
	 * @param serverPort the server port
	 * @param serverPassword the server password
	 * @param appName the name of the application
	 */
	public ClientConfiguration(String serverAddress, int serverPort, String serverPassword, String appName)
	{
		if(appName.equals("")) throw new IllegalArgumentException("The app must have a name");
		
		if(serverAddress.equals("") || serverPort == 0) throw new IllegalArgumentException("Please make sure you entered a valid host name and port name");
		
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
		this.serverPassword = serverPassword;
		
		Settings.setAppName(appName);
	}

	/**
	 * Gets the server address.
	 *
	 * @return the server address
	 */
	public String getServerAddress()
	{
		return serverAddress;
	}

	/**
	 * Gets the server port.
	 *
	 * @return the server port
	 */
	public int getServerPort()
	{
		return serverPort;
	}

	/**
	 * Gets the server password.
	 *
	 * @return the server password
	 */
	public String getServerPassword()
	{
		return serverPassword;
	}
}
