package client;

import helpers.*;

import java.io.*;
import java.net.*;
import java.security.PublicKey;
import java.util.*;

import network.*;
import statics.Settings;
import util.*;
import exceptions.OwnzoneException;

/**
 * This class constitutes a network connection to a server running the Server framework. <br>
 * The class handles managing the connection, sending requests to the server and interacting with the {@link ServerRequestManager}
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ClientConnection
{
	private final String NAME = this.getClass().getSimpleName();

	private static ClientConnection clientInstance;

	private ServerRequestManager serverRequestManager;

	private Socket s;

	private ObjectOutputStream outStream;

	private ObjectInputStream inStream;

	private UUID clientId;

	private boolean connected;

	/**
	 * Instantiates the client connection.
	 */
	private ClientConnection()
	{
		connected = false;
	}

	/**
	 * Gets the singleton client instance.
	 *
	 * @return the client instance
	 */
	public static ClientConnection getClientInstance()
	{
		if (clientInstance == null) clientInstance = new ClientConnection();

		return clientInstance;
	}

	// public void connect(String host, int port, String serverPassword) throws IOException, ClassNotFoundException, OwnzoneException
	/**
	 * Connect to the specified server.
	 *
	 * @param clientConfiguration
	 *            the client configuration containing connection-critical data
	 * @throws IOException
	 *             signals that an I/O exception has occurred
	 * @throws ClassNotFoundException
	 *             indicates a missing class
	 * @throws OwnzoneException
	 *             indicates a problem with connecting to the server
	 */
	public void connect(ClientConfiguration clientConfiguration) throws IOException, ClassNotFoundException, OwnzoneException
	{
		Settings.setServerAddress(clientConfiguration.getServerAddress());
		Settings.setServerPassword(clientConfiguration.getServerPassword());
		Settings.setServerPort(clientConfiguration.getServerPort());

		if (!isConnected())
		{
			long timeStamp = System.currentTimeMillis();

			s = new Socket();
			s.setSoTimeout(0);
			s.connect(new InetSocketAddress(Settings.getServerAddress(), Settings.getServerPort()));

			outStream = new ObjectOutputStream(s.getOutputStream());
			inStream = new ObjectInputStream(s.getInputStream());

			serverRequestManager = new ServerRequestManager();
			serverRequestManager.finalize(outStream, inStream);

			UUID connectRequestId = sendServerRequest(ServerRequestFactory.buildConnectRequest());

			ConsoleHelper.announce(NAME, "Attempting to connect to the server at " + Settings.getServerAddress() + ":" + Settings.getServerPort());

			while (serverRequestManager.getRequest(connectRequestId).getServerRequestStatus() != ServerRequestStatus.COMPLETE)
			{
				try
				{
					Thread.sleep(10);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			NetworkPackage networkPackageReply = serverRequestManager.getRequest(connectRequestId).getResultNetworkPackage();

			if (!networkPackageReply.failed())
			{
				String properties = "";
				for (String s : networkPackageReply.getPropertyIdentifiers())
				{
					properties = properties + s + ", ";
				}
				properties = properties.substring(0, properties.length() - 2);

				ConsoleHelper.announce(NAME, "Got the following data from server: " + properties);

				UUID clientId = networkPackageReply.getPropertyValue("ClientId");
				if (clientId != null)
				{
					this.clientId = clientId;
					ConsoleHelper.announce(NAME, "Got client ID from server: " + clientId);
					connected = true;
				}
				else
				{
					ConsoleHelper.announce(NAME, "No client ID returned from server.");
					connected = false;
				}

				PublicKey publicKey = networkPackageReply.getPropertyValue("PublicKey");
				if (publicKey != null)
				{
					CryptoHelper.setServerPublicKey(publicKey);
					connected = true;
				}
				else
				{
					ConsoleHelper.announce(NAME, "No encryption key returned from server.");
					connected = false;
				}
			}
			else
			{
				ConsoleHelper.announce(NAME, "Failed to connect: " + networkPackageReply.getException().getSimpleMessage());
			}

			if (connected)
			{
				ConsoleHelper.announce(NAME, "Connected and ready after " + (System.currentTimeMillis() - timeStamp) + " ms.");
			}
			else
			{
				serverRequestManager.shutdown();
				ConsoleHelper.announce(NAME, "End of life. Bye.");
			}
		}
	}

	// private void test() throws IOException, OwnzoneException
	// {
	// ServerRequest sreq = new ServerRequest("WololoTask");
	// sreq.putProperty(new DynamicProperty<String>("name", "", "Lasse", false));
	// UUID testId = sendServerRequest(sreq);
	//
	// long l = System.currentTimeMillis();
	// ServerRequest sr = serverRequestManager.waitForCompleteRequest(testId);
	//
	// NetworkPackage npReply = sr.getResultPackage();
	//
	// System.out.println("Server replied: " + npReply.getPropertyValue("wololo") + " after " + (System.currentTimeMillis() - l) + " ms");
	// }
	//
	// private void testSecureString() throws IOException, OwnzoneException
	// {
	// ServerRequest sreq = new ServerRequest("SecureStringTask");
	// sreq.putProperty(new DynamicProperty<SecureString>("SecureString", "", new SecureString("Nicolaj har v�ret v�k mange gange", CryptoHelper.getServerPublicKey()), false));
	// UUID testId = sendServerRequest(sreq);
	//
	// // long l = System.currentTimeMillis();
	// // ServerRequest sr = serverRequestManager.waitForCompleteRequest(testId);
	// //
	// // NetworkPackage npReply = sr.getResultPackage();
	// //
	// // SecureString ss = npReply.getPropertyValue("SecureReply");
	// //
	// // System.out.println("Server replied: " + ss.getValue(CryptoHelper.getPrivateEncryptionKey()) + " after " + (System.currentTimeMillis() - l) + " ms");
	// }

	/**
	 * Send a server request to the server.
	 *
	 * @param serverRequest
	 *            the server request
	 * @return the UUID that is the reference to this request
	 * @throws IOException
	 *             signals that an I/O exception has occurred.
	 * @throws OwnzoneException
	 *             an exception indicating a problem with sending the server request
	 */
	public UUID sendServerRequest(ServerRequest serverRequest) throws IOException, OwnzoneException
	{
		if (clientId != null) serverRequest.setClientId(clientId);

		serverRequestManager.addRequest(serverRequest);

		return serverRequest.getIdentifier();
	}

	/**
	 * Gets the output stream of the connection.
	 *
	 * @return the out stream
	 */
	public ObjectOutputStream getOutStream()
	{
		return outStream;
	}

	/**
	 * Gets the input stream of the connection.
	 *
	 * @return the in stream
	 */
	public ObjectInputStream getInStream()
	{
		return inStream;
	}

	/**
	 * Checks if is connected.
	 *
	 * @return true, if is connected
	 */
	public boolean isConnected()
	{
		return connected;
	}

	/**
	 * Gets a server request that has been sent off to the server.
	 *
	 * @param identifier
	 *            the identifier of the request
	 * @return the server request
	 */
	public ServerRequest getServerRequest(UUID identifier)
	{
		return serverRequestManager.getRequest(identifier);
	}

	/**
	 * Wait for server request completion and get server request.
	 *
	 * @param identifier
	 *            the identifier of the request
	 * @return the server request
	 */
	public ServerRequest waitAndGetServerRequest(UUID identifier)
	{
		return serverRequestManager.waitForCompleteRequest(identifier);
	}

	/**
	 * Gets all pending (not complete) requests.
	 * 
	 * @return all pending requests
	 */
	public ArrayList<UUID> getAllPendingRequests()
	{
		return serverRequestManager.getAllPendingRequests();
	}

	/**
	 * Gets all server request (no matter the status).
	 * 
	 * @return all server requests
	 */
	public ArrayList<ServerRequest> getAllServerRequests()
	{
		return serverRequestManager.getAllRequests();
	}
	
	public void disconnect()
	{
		try
		{
			sendServerRequest(new ServerRequest(RequestType.DISCONNECT));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (OwnzoneException e)
		{
			e.printStackTrace();
		}
		
		ConsoleHelper.announce(NAME, "Connection peacefully closed.");
	}
}
