package helpers;

import java.io.*;

import exceptions.OwnzoneException;
import statics.Settings;

/**
 * This class helps the framework with file-related things.
 * 
 * Cannot be instantiated; access statically.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class FileHelper
{
	private static String userSpace = getAppDirectory() + "\\userspace\\";

	/**
	 * Gets the app directory (the file location at which the framework operates).
	 *
	 * @return the app directory
	 */
	public static String getAppDirectory()
	{
		return System.getenv("APPDATA") + "\\" + Settings.getAppName();
	}

	/**
	 * Not yet implemented.
	 */
	public static void writeObjectToFile(Object object, String fileName)
	{

	}

	/**
	 * Read object from file.
	 *
	 * @param fileName the file name
	 * @return the object
	 */
	public static Object readObjectFromFile(String fileName)
	{
		return null;
	}

	/**
	 * Gets the path for the{@code settings.ini} file used to configure the framework.
	 *
	 * @return the settings path
	 * @throws OwnzoneException an exception indicating a problem with reading the settings
	 * @throws IOException signals that an I/O exception has occurred
	 */
	public static String getSettingsPath() throws OwnzoneException, IOException
	{
		String settingsFile = getAppDirectory() + "\\settings.ini";
		File settings = new File(settingsFile);
		
		if (settings.exists())
		{
			try
			{
				BufferedReader br = new BufferedReader(new FileReader(settings));
				
				if (br.readLine() == null) 
				{
					throw new OwnzoneException("Settings file not yet set up, it's located at: " + settings.getAbsolutePath());
				}
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
				throw new OwnzoneException("Couldn't read settings file", e);
			}
			catch (IOException e)
			{
				e.printStackTrace();
				throw new OwnzoneException("Couldn't read settings file", e);
			}
			
			return settingsFile;
		}
		else
		{
			settings.createNewFile();
			throw new OwnzoneException("Settings file not yet set up, it's located at: " + settings.getAbsolutePath());
		}
	}

	/**
	 * Read a file from the file system.
	 *
	 * @param path the path of the file
	 * @return the file
	 */
	public static File readFile(String path)
	{
		return new File(path);
	}
}
