package helpers;

import java.io.*;
import java.util.ArrayList;

/**
 * The purpose of this class is to provide a consistent, easy-to-use console and log output for the developer.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ConsoleHelper
{
	private static boolean enabled = false;
	
	private static boolean timestampEnabled = true;
	
	private static boolean persistent = false;

	private static FileWriter fw = null;

	private static ArrayList<String> instanceLog;

	private ConsoleHelper()
	{

	}

	static
	{
		instanceLog = new ArrayList<>();
	}

	/**
	 * Disable the console helper (no output will be carried out).
	 */
	public static void disable()
	{
		enabled = false;
	}

	/**
	 * Enable the console helper.
	 */
	public static void enable()
	{
		enabled = true;
	}

	/**
	 * Disable timestamp.
	 */
	public static void disableTimestamp()
	{
		timestampEnabled = false;
	}

	/**
	 * Enable timestamp.
	 */
	public static void enableTimestamp()
	{
		timestampEnabled = true;
	}

	/**
	 * Enable persistence (writing data to the log file).
	 */
	public static void enablePersistence()
	{
		persistent = true;
	}

	/**
	 * Disable persistence (writing data to the log file).
	 */
	public static void disablePersistence()
	{
		persistent = false;
	}

	/**
	 * Prints a line to the console and, if persistence is enabled, to the log file.
	 *
	 * @param output the output
	 */
	public static void println(String output)
	{
		if (enabled)
		{
			if (timestampEnabled)
			{
				output = "[" + DateTimeHelper.getCurrentTime() + "]: " + output;
			}

			System.out.println(output);

			if (persistent)
			{
				writeToLogFile(output);
			}
		}

	}

	/**
	 * Prints a line to the console and, if persistence is enabled, to the log file, with a "sender" name (usually the class the announce was requested from).
	 *
	 * @param sender the sender (usually a class name)
	 * @param output the output
	 */
	public static void announce(String sender, String output)
	{
		if (enabled)
		{
			if (timestampEnabled)
			{
				output = "[" + sender + " \u2014 " + DateTimeHelper.getCurrentTime() + "]: " + output;
			}
			else
			{
				output = "[" + sender + "]: " + output;
			}

			System.out.println(output);

			if (persistent)
			{
				writeToLogFile(output);
			}
		}
	}

	/**
	 * Prints a bigger, more visible line in the console.
	 *
	 * @param output the output
	 */
	public static void printlnBig(String output)
	{
		if (enabled)
		{
			output = " --- " + output.toUpperCase() + " --- ";

			System.out.println(output);

			if (persistent)
			{
				writeToLogFile(output);
			}
		}
	}

	/**
	 * Write an entry to the log file.
	 *
	 * @param output the line to be added to the file
	 */
	public static void writeToLogFile(String output)
	{
		try
		{
			if (instanceLog == null) instanceLog = new ArrayList<String>();
			
			synchronized (instanceLog)
			{
				instanceLog.add(output);
			}

			File logFile = new File(FileHelper.getAppDirectory() + "\\log.txt");

			if (!logFile.exists()) logFile.createNewFile();

			if (fw == null) fw = new FileWriter(logFile, true);
			fw.write(output);
			fw.write("\n");
			fw.flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Gets the instance log representing the log entries since the server was started.
	 *
	 * @return the instance log
	 */
	public static ArrayList<String> getInstanceLog()
	{
		synchronized (instanceLog)
		{
			return instanceLog;
		}
	}
}
