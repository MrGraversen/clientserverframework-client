package helpers;
import java.io.*;
import java.security.*;

import javax.crypto.Cipher;

import exceptions.OwnzoneException;

/**
 * The purpose of this class is to provide generic cryptographic facilities for the framework.
 * This includes hashing, RSA encryption using public and private key pairs, and converting bytes to hexadecimal values (and vice versa).
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class CryptoHelper
{
	private final static String NAME = CryptoHelper.class.getSimpleName();

	private static final String ALGORITHM = "RSA";

	private static final String PRIVATE_KEY_FILE = FileHelper.getAppDirectory() + "\\keys\\private.key";

	private static final String PUBLIC_KEY_FILE = FileHelper.getAppDirectory() + "\\keys\\public.key";

	private static final int KEY_SIZE = 2048;

	private static PublicKey serverPublicKey = null;

	private CryptoHelper()
	{
	}

	static
	{
		if (!keysExists())
		{
			generateKeys();
		}
	}

	/**
	 * Generate encryption keys (public and private) for RSA encryption.
	 */
	private static void generateKeys()
	{
		ConsoleHelper.announce(NAME, "Generating encryption keys.");
		try
		{
			final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
			keyGen.initialize(KEY_SIZE); // input size = (n / 8) - 11
			final KeyPair key = keyGen.generateKeyPair();

			File privateKeyFile = new File(PRIVATE_KEY_FILE);
			File publicKeyFile = new File(PUBLIC_KEY_FILE);

			// Create files to store public and private key
			if (privateKeyFile.getParentFile() != null)
			{
				privateKeyFile.getParentFile().mkdirs();
			}
			privateKeyFile.createNewFile();

			if (publicKeyFile.getParentFile() != null)
			{
				publicKeyFile.getParentFile().mkdirs();
			}
			publicKeyFile.createNewFile();

			// Saving the Public key in a file
			ObjectOutputStream publicKeyOS = new ObjectOutputStream(new FileOutputStream(publicKeyFile));
			publicKeyOS.writeObject(key.getPublic());
			publicKeyOS.close();

			// Saving the Private key in a file
			ObjectOutputStream privateKeyOS = new ObjectOutputStream(new FileOutputStream(privateKeyFile));
			privateKeyOS.writeObject(key.getPrivate());
			privateKeyOS.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Perform a check indicating whether or not encryption keys are present.
	 *
	 * @return true, if successful
	 */
	private static boolean keysExists()
	{
		File privateKey = new File(PRIVATE_KEY_FILE);
		File publicKey = new File(PUBLIC_KEY_FILE);

		return privateKey.exists() && publicKey.exists();
	}

	/**
	 * Gets the public encryption key.
	 *
	 * @return the public encryption key
	 * @throws OwnzoneException an exception indicating an error with the retrieval process
	 */
	public static PublicKey getPublicEncryptionKey() throws OwnzoneException
	{
		try
		{
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PUBLIC_KEY_FILE));
			final PublicKey publicKey = (PublicKey) inputStream.readObject();
			inputStream.close();

			return publicKey;
		}
		catch (Exception e)
		{
			throw new OwnzoneException("Could not get public encryption key.", e);
		}
	}

	/**
	 * Gets the private encryption key.
	 *
	 * @return the private encryption key
	 * @throws OwnzoneException an exception indicating an error with the retrieval process
	 */
	public static PrivateKey getPrivateEncryptionKey() throws OwnzoneException
	{
		try
		{
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PRIVATE_KEY_FILE));
			final PrivateKey privateKey = (PrivateKey) inputStream.readObject();
			inputStream.close();

			return privateKey;
		}
		catch (Exception e)
		{
			throw new OwnzoneException("Could not get private encryption key.", e);
		}
	}

	public static void setServerPublicKey(PublicKey publicKey)
	{
		CryptoHelper.serverPublicKey = publicKey;
	}

	public static PublicKey getServerPublicKey()
	{
		return serverPublicKey;
	}

	/**
	 * Attempts to encrypt a string using RSA encrypt.
	 *
	 * @param plainText the plain text to be encrypted
	 * @param publicKey the public key for encryption
	 * @return the byte array representing the encrypted data
	 * @throws OwnzoneException an exception containing data as to why the encryption failed
	 */
	public static byte[] rsaEncrypt(String plainText, PublicKey publicKey) throws OwnzoneException
	{
		if (plainText.length() > ((KEY_SIZE / 8) - 11)) throw new IllegalArgumentException("Input size must be at most " + ((KEY_SIZE / 8) - 11) + " bytes (characters).");

		byte[] cipherOutput = null;
		try
		{
			// ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PUBLIC_KEY_FILE));
			// final PublicKey publicKey = (PublicKey) inputStream.readObject();

			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(ALGORITHM);

			// encrypt the plain text using the public key
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			cipherOutput = cipher.doFinal(plainText.getBytes());

			// inputStream.close();
		}
		catch (Exception ex)
		{
			throw new OwnzoneException("Could not encrypt.", ex);
		}

		return cipherOutput;
	}

	/**
	 * Attempts to decrypt a byte array using RSA.
	 *
	 * @param cipherInput the cipher input
	 * @param privateKey the private key for decryption
	 * @return the string representing the decrypted data
	 * @throws OwnzoneException an exception containing data as to why the decryption failed
	 */
	public static String rsaDecrypt(byte[] cipherInput, PrivateKey privateKey) throws OwnzoneException
	{
		byte[] dectyptedText = null;
		try
		{
			// ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(PRIVATE_KEY_FILE));
			// final PrivateKey privateKey = (PrivateKey) inputStream.readObject();

			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance(ALGORITHM);

			// decrypt the text using the private key
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			dectyptedText = cipher.doFinal(cipherInput);

			// inputStream.close();
		}
		catch (Exception ex)
		{
			throw new OwnzoneException("Could not decrypt.", ex);
		}

		return new String(dectyptedText);
	}

	/**
	 * Computes a hash from a string of text. Useful for, for example, password security.<br>
	 * Supports the following algorithms: <a href="https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest">https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest</a>
	 * <br>This method can hash for a specified number of times to enhance secuity and avoid rainbow-table attacks.
	 *
	 * @param instance the algorithm instance, for example {@code SHA-512}
	 * @param plainText the plain text to be hashed
	 * @param	iterations	the number of times to apply the hash (high iterations is slower, but more secure)
	 * @return string representing the hash in hexadecimal
	 */
	public static String computeHash(String instance, String plainText, int iterations)
	{
		String output = plainText;

		for (int i = 0; !(i == iterations); i++)
		{
			output = computeHash(instance, output);
		}

		return output;
	}

	/**
	 * Computes a hash from a string of text. Useful for, for example, password security.<br>
	 * Supports the following algorithms: <a href="https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest">https://docs.oracle.com/javase/7/docs/technotes/guides/security/StandardNames.html#MessageDigest</a>
	 *
	 * @param instance the algorithm instance, for example {@code SHA-512}
	 * @param plainText the plain text to be hashed
	 * @return string representing the hash in hexadecimal
	 */
	public static String computeHash(String instance, String plainText)
	{
		MessageDigest md;

		try
		{
			md = MessageDigest.getInstance(instance);
			md.update(plainText.getBytes());

			byte byteData[] = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++)
			{
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();
		}
		catch (Exception e)
		{
			return e.toString();
		}
	}

	/**
	 * Hex to bytes.
	 *
	 * @param hex the hex
	 * @return the byte[]
	 */
	public static byte[] hexToBytes(String hex)
	{
		return hexToBytes(hex.toCharArray());
	}

	/**
	 * Converts hexadecimal chars into byte values.
	 *
	 * @param hex char array of hexadecimal characters
	 * @return a byte array representing the converted characters
	 */
	private static byte[] hexToBytes(char[] hex)
	{
		if (hex.length % 2 != 0) throw new IllegalArgumentException("Must pass an even number of characters.");

		int length = hex.length >> 1;
		byte[] raw = new byte[length];
		for (int o = 0, i = 0; o < length; o++)
		{
			raw[o] = (byte) ((getHexCharValue(hex[i++]) << 4) | getHexCharValue(hex[i++]));
		}
		return raw;
	}

	private static byte getHexCharValue(char c)
	{
		if (c >= '0' && c <= '9') return (byte) (c - '0');
		if (c >= 'A' && c <= 'F') return (byte) (10 + c - 'A');
		if (c >= 'a' && c <= 'f') return (byte) (10 + c - 'a');
		throw new IllegalArgumentException("Invalid hex character");
	}
	
	/**
	 * Converts a standard byte array into a string of hexadecimal characters.
	 *
	 * @param bytes	byte array to be converted
	 * @return the hexadecimal string
	 */
	public static String bytesToHex(byte[] bytes)
	{
		final char[] hexArray =
		{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++)
		{
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}