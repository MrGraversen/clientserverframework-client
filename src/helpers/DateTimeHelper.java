package helpers;
import java.util.*;


/**
 * This class assists the framework with various date and time related tasks.
 * This ranges from converting to and from unix time, and presenting date- and timestamps in a human-readable way.
 * 
 * Cannot be instantiated; access statically.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class DateTimeHelper
{
	private DateTimeHelper()
	{
	}

	/**
	 * Gets the current unix time.
	 * Calculated the following way:
	 * {@code System.currentTimeMillis() / 1000L}
	 *
	 * @return the current unix timestamp
	 */
	public static long getCurrentUnixTime()
	{
		return System.currentTimeMillis() / 1000L;
	}

	/**
	 * Gets the system's millisecond timestamp.
	 *
	 * @return the time stamp
	 */
	public static long getTimeStamp()
	{
		return System.currentTimeMillis();
	}

	/**
	 * Gets the current date in {@code DD-MM-YYYY} format.
	 *
	 * @return the current date
	 */
	public static String getCurrentDate()
	{
		return getDate(getCurrentUnixTime());
	}

	/**
	 * Gets the current time in {@code HH:MM:SS} format.
	 *
	 * @return the current time
	 */
	public static String getCurrentTime()
	{
		return getTime(getCurrentUnixTime());
	}

	/**
	 * Converts a unix timestamp into a human-readable date, in {@code DD-MM-YYYY} format.
	 *
	 * @param unixTimeStamp the unix time stamp
	 * @return the date
	 */
	public static String getDate(long unixTimeStamp)
	{
		Calendar myDate = Calendar.getInstance();
		myDate.setTimeInMillis(unixTimeStamp * 1000);

		String day = null;
		String month = null;
		String year = null;

		if (myDate.get(Calendar.DAY_OF_MONTH) < 10)
		{
			day = "0" + myDate.get(Calendar.DAY_OF_MONTH);
		}
		else
		{
			day = "" + myDate.get(Calendar.DAY_OF_MONTH);
		}

		if ((myDate.get(Calendar.MONTH) + 1) < 10)
		{
			month = "0" + (myDate.get(Calendar.MONTH) + 1);
		}
		else
		{
			month = "" + (myDate.get(Calendar.MONTH) + 1);
		}

		year = String.valueOf(myDate.get(Calendar.YEAR));

		//return day + "-" + month + "-" + year;
		return year + "-" + month + "-" + day;
	}

	/**
	 * Converts a unix timestamp into a human-readable time, in {@code HH-MM-SS} format.
	 *
	 * @param unixTimeStamp the unix time stamp
	 * @return the time
	 */
	public static String getTime(long unixTimeStamp)
	{
		Calendar myDate = Calendar.getInstance();
		myDate.setTimeInMillis(unixTimeStamp * 1000);

		String hour = null;
		String minute = null;
		String second = null;

		if (myDate.get(Calendar.HOUR_OF_DAY) < 10)
		{
			hour = "0" + myDate.get(Calendar.HOUR_OF_DAY);
		}
		else
		{
			hour = "" + myDate.get(Calendar.HOUR_OF_DAY);
		}

		if (myDate.get(Calendar.MINUTE) < 10)
		{
			minute = "0" + myDate.get(Calendar.MINUTE);
		}
		else
		{
			minute = "" + myDate.get(Calendar.MINUTE);
		}

		if (myDate.get(Calendar.SECOND) < 10)
		{
			second = "0" + myDate.get(Calendar.SECOND);
		}
		else
		{
			second = "" + myDate.get(Calendar.SECOND);
		}

		return hour + ":" + minute + ":" + second;
	}
}