package statics;

/**
 * This class handles all the settings for the client.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class Settings
{
	private static String serverPassword = "";
	private static String appName = "";
	private static String serverAddress = "";
	private static int serverPort = 0;

	private Settings()
	{
	}

	public static String getServerPassword()
	{
		return serverPassword;
	}

	public static void setServerPassword(String serverPassword)
	{
		Settings.serverPassword = serverPassword;
	}
	
	public static String getAppName()
	{
		return appName;
	}

	public static void setAppName(String appName)
	{
		Settings.appName = appName;
	}

	public static String getServerAddress()
	{
		return serverAddress;
	}

	public static void setServerAddress(String serverAddress)
	{
		Settings.serverAddress = serverAddress;
	}

	public static int getServerPort()
	{
		return serverPort;
	}

	public static void setServerPort(int serverPort)
	{
		Settings.serverPort = serverPort;
	}
}
