package abstracts;
import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import util.DynamicProperty;

/**
 * This abstract class enables subclasses to act as an entity able to hold DynamicProperty objects, thus making it a generic data holder for whatever its purpose might be.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public abstract class DynamicPropertyEntity implements Serializable
{
	private ConcurrentHashMap<String, DynamicProperty<?>> properties;
	
	private ArrayList<String> propertyIdentifiers;

	/**
	 * Instantiates a new dynamic property entity, instantiating the data collections so they're ready to use.
	 */
	public DynamicPropertyEntity()
	{
		this.properties = new ConcurrentHashMap<>();
		this.propertyIdentifiers = new ArrayList<String>();
	}

	/**
	 * Put a DynamicProperty into the data holder.
	 *
	 * @param property the property object
	 */
	public void putProperty(DynamicProperty<?> property)
	{
		if (property.isValid())
		{
			properties.put(property.getIdentifier(), property);
			propertyIdentifiers.add(property.getIdentifier());
		}
		else
		{
			throw new InvalidParameterException("The value or identifier of the property must not be null.");
		}
	}

	/**
	 * Gets a DynamicProperty from the data holder.
	 *
	 * @param identifier the identifier of the object
	 * @return the property
	 */
	public DynamicProperty<?> getProperty(String identifier)
	{
		if (!properties.containsKey(identifier)) return null;
		return properties.get(identifier);
	}

	/**
	 * Gets the property value of a property.
	 * <br>This is alternative to calling:
	 * <br>{@code DynamicProperty<String> myProperty = getProperty("myIdentifier");}
	 * <br>{@code String myValue = myProperty.getValue();}
	 * 
	 *
	 * @param <T> the generic type
	 * @param identifier the identifier of the object
	 * @return the property's value (of the generic type)
	 */
	public <T> T getPropertyValue(String identifier)
	{
		if (!properties.containsKey(identifier)) return null;
		return (T) getProperty(identifier).getValue();
	}

	/**
	 * Removes a property from the data holder.
	 *
	 * @param identifier the identifier of the object
	 */
	public void removeProperty(String identifier)
	{
		properties.remove(identifier);
	}

	/**
	 * Gets all the identifiers of objects held by this data holder.
	 *
	 * @return the property identifiers
	 */
	public ArrayList<String> getPropertyIdentifiers()
	{
		return propertyIdentifiers;
	}

	/**
	 * Gets the all properties from this data holder in an ArrayList.
	 *
	 * @return the all properties
	 */
	public ArrayList<DynamicProperty<?>> getAllProperties()
	{
		ArrayList<DynamicProperty<?>> listOfProperties = new ArrayList<DynamicProperty<?>>();

		for (Entry<String, DynamicProperty<?>> dynamicProperty : properties.entrySet())
		{
			listOfProperties.add(dynamicProperty.getValue());
		}

		return listOfProperties;
	}

	/**
	 * Copy all properties to this {@code DynamicPropertyEntity} from another {@code DynamicPropertyEntity}.
	 *
	 * @param dynamicPropertyEntity the dynamic property entity
	 */
	public void copyFromEntity(DynamicPropertyEntity dynamicPropertyEntity)
	{
		for (DynamicProperty<?> dp : dynamicPropertyEntity.getAllProperties())
		{
			putProperty(dp);
		}
	}

	/**
	 * Copy properties from an ArrayList of {@code DynamicProperty} into this data holder.
	 *
	 * @param dynamicPropertyList the dynamic property list
	 */
	public void copyProperties(ArrayList<DynamicProperty<?>> dynamicPropertyList)
	{
		for (DynamicProperty<?> dynamicProperty : dynamicPropertyList)
		{
			putProperty(dynamicProperty);
		}
	}
}
