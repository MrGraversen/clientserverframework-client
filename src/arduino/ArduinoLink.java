package arduino;
import exceptions.OwnzoneException;
import gnu.io.*;
import helpers.ConsoleHelper;

import java.io.*;
import java.util.*;

/**
 * This class constitutes a simple serial link between the PC and an Arduino connected through a communications port.
 * <br>The class establishes a pair between the two hardware units and allows for easy serial communication to and from the Arduino.
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class ArduinoLink implements SerialPortEventListener
{
	private final String NAME = this.getClass().getSimpleName();

	private boolean link;
	
	private String[] portNames;

	private int dataRate;
	
	private int timeOut;

	private BufferedReader input;
	
	private OutputStream output;

	private SerialPort serialPort;

	private LinkedList<String> inputList;

	/**
	 * Instantiates a new ArduinoLink object, getting it ready to establish a link.
	 * @see ArduinoLink#establishLink(int, int)
	 */
	public ArduinoLink()
	{
		serialPort = null;

		link = false;

		portNames = new String[99];

		for (int i = 1; i < 100; i++)
		{
			portNames[i - 1] = "COM" + i;
		}

		inputList = new LinkedList<String>();
	}

	/**
	 * Checks if is link established.
	 *
	 * @return true, if is link established
	 */
	public boolean isLinkEstablished()
	{
		return link;
	}

	/**
	 * Establish a link between the computer and the Arduino.
	 *
	 * @param baudRate the communication bit rate (must be the same on both Arduino and here), typically 9600 bit/s
	 * @param timeOut the time out duration
	 * @throws OwnzoneException an exception indicating a failure in establishing the link
	 */
	public void establishLink(int baudRate, int timeOut) throws OwnzoneException
	{
		this.dataRate = baudRate;
		this.timeOut = timeOut;

		try
		{
			CommPortIdentifier portId = null;
			Enumeration<?> portEnum = CommPortIdentifier.getPortIdentifiers();

			// Enumerate system ports and try connecting to Arduino over each
			while (portId == null && portEnum.hasMoreElements())
			{
				// Iterate through your host computer's serial port IDs
				CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();

				for (String portName : portNames)
				{
					if (currPortId.getName().equals(portName) || currPortId.getName().startsWith(portName))
					{
						// Try to connect to the Arduino on this port
						// Open serial port
						serialPort = (SerialPort) currPortId.open("ClientCore", timeOut);
						portId = currPortId;
						ConsoleHelper.announce(NAME, "Connected on port: " + currPortId.getName());
					}
				}
			}

			if (portId == null || serialPort == null)
			{
				throw new OwnzoneException("Could not connect to arduino.");
			}

			// set port parameters
			serialPort.setSerialPortParams(dataRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);

			// Give the Arduino some time
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException ie)
			{
			}

			output = serialPort.getOutputStream();

			link = true;

			ConsoleHelper.announce(NAME, "Link established.");
		}
		catch (OwnzoneException e)
		{
			throw e;
		}
		catch (Exception e)
		{
			throw new OwnzoneException("Arduino error; see primary exception.", e);
		}
	}

	/* (non-Javadoc)
	 * @see gnu.io.SerialPortEventListener#serialEvent(gnu.io.SerialPortEvent)
	 */
	@Override
	public void serialEvent(SerialPortEvent spe)
	{
		try
		{
			if (spe.getEventType() == SerialPortEvent.DATA_AVAILABLE)
			{
				if (input == null)
				{
					input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
				}

				if (input.ready())
				{
					String inputLine = input.readLine();
					inputList.add(inputLine);
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Wait for input from the Arduino and return that input, once received.
	 *
	 * @return the latest input 
	 */
	public String waitForInput()
	{
		int n = inputList.size();

		while (inputList.size() == n)
		{
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		return getLatestInput();
	}
	
	public String waitForInput(int timeOut)
	{
		int duration = 0;
		
		int n = inputList.size();

		while (inputList.size() == n)
		{
			if(duration > timeOut) return null;
			
			try
			{
				Thread.sleep(10);
				duration += 10;
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		return getLatestInput();
	}

	/**
	 * Gets the latest input from the Arduino.
	 *
	 * @return the latest input
	 */
	public String getLatestInput()
	{
		return inputList.getLast();
	}

	/**
	 * Gets all the inputs from the Arduino.
	 *
	 * @return the inputs
	 */
	public ArrayList<String> getInputs()
	{
		return new ArrayList<String>(inputList);
	}

	/**
	 * Send data to the Arduino. This data can be read during a serial read on the Arduino.
	 *
	 * @param data the data
	 * @param lineFeed whether or not a line feed should be applied to the end of the serial string
	 * @throws OwnzoneException an exception indicating a communication failure
	 */
	public void sendData(String data, boolean lineFeed) throws OwnzoneException
	{
		if (link)
		{
			if (lineFeed) data = data + "\n";

			try
			{
				output.write(data.getBytes());
				output.flush();
			}
			catch (Exception e)
			{
				throw new OwnzoneException("Could not send data; see primary exception.", e);
			}
		}
		else
		{
			throw new OwnzoneException("Link not established.");
		}
	}

	/**
	 * Gets the baud rate (the communication speed, in bit/s).
	 *
	 * @return the baud rate
	 */
	public int getBaudRate()
	{
		return dataRate;
	}

	/**
	 * Gets the time out duration, in milliseconds.
	 *
	 * @return the time out
	 */
	public int getTimeOut()
	{
		return timeOut;
	}

}
