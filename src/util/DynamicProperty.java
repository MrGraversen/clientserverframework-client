package util;

import java.io.Serializable;
import java.security.InvalidParameterException;

import abstracts.DynamicPropertyEntity;

/**
 * Constitutes a property that can hold any abstract data type. These properties are sent over the network, and thus enables the developer to easily transfer any type and value from client to server, and from server to client.
 *
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 * @param <T> the generic type indicating which type this {@code DynamicProperty} is.
 */
public class DynamicProperty<T> implements Serializable
{
	private String identifier;
	
	private String description;
	
	private T value;
	
	private boolean invokeOnce;
	
	private boolean invoked;

	/**
	 * Instantiates a new dynamic property.
	 * <br>If the {@code invokeOnce} flag is set to {@code true}, the value will be set to {@code null} (and therefore garbage collected), allowing sensitive data to be released from the system.
	 * <br>Consider using {@link SecureString} if you need to transfer sensitive data. Alternatively, use {@code CryptoHelper} to hash data.
	 *
	 * @param identifier the identifier, used for grabbing the property out of a {@link DynamicPropertyEntity}
	 * @param description the description of the property
	 * @param value the value
	 * @param invokeOnce whether or not the value can be invoked once
	 */
	public DynamicProperty(String identifier, String description, T value, boolean invokeOnce)
	{
		if(identifier.isEmpty()) throw new InvalidParameterException("The identifier must be something.");
		if(value == null) throw new InvalidParameterException("The value must not be null.");
		
		this.identifier = identifier;
		this.description = description;
		this.value = value;
		this.invokeOnce = invokeOnce;
	}

	/**
	 * Checks if the property is valid.
	 *
	 * @return true, if is valid
	 */
	public boolean isValid()
	{
		return value != null;
	}

	/**
	 * Gets the description of the property.
	 *
	 * @return the description
	 */
	public String getDescription()
	{
		return description.equals("") ? description : "(No description available)";
	}

	/**
	 * Gets the value of the property, of the type {@code T}.
	 *
	 * @return the value
	 */
	public T getValue()
	{
		if (invokeOnce && !invoked)
		{
			invoked = true;
			T tempValue = value;
			value = null;
			return tempValue;
		}
		
		return value;
	}

	/**
	 * Gets the identifier of the property, used to identify the property in a {@link DynamicPropertyEntity}.
	 *
	 * @return the identifier
	 */
	public String getIdentifier()
	{
		return identifier;
	}
}
