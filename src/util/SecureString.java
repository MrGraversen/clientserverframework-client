package util;
import helpers.CryptoHelper;

import java.io.Serializable;
import java.security.*;

import exceptions.OwnzoneException;

/**
 * Constitutes a <b>secure</b> string, encrypted using RSA (assymetrically).
 * <br>The security is specified by the bit-size of the encryption keys (by default 2048 bit).
 * <br>Once encrypted using the recipient's <i>public key</i>, it can only be decrypted using the recipient's <i>private</i> key. 
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class SecureString implements Serializable
{
	private final byte[] value;

	/**
	 * Instantiates a new secure string.
	 *
	 * @param text the text to be encrypted
	 * @param publicKey the public key
	 * @throws OwnzoneException an exception indicating a problem with the encryption
	 */
	public SecureString(String text, PublicKey publicKey) throws OwnzoneException
	{
		value = CryptoHelper.rsaEncrypt(text, publicKey);
	}

	/**
	 * Gets the decrypted value of the encrypted data.
	 *
	 * @param privateKey the private key
	 * @return the decrypted value
	 * @throws OwnzoneException an exception indicating a problem with the encryption
	 */
	public String getValue(PrivateKey privateKey) throws OwnzoneException
	{
		return CryptoHelper.rsaDecrypt(value, privateKey);
	}
}
