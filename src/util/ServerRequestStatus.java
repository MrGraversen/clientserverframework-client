package util;

import network.ServerRequest;

/**
 * Indicates the status of a {@link ServerRequest},
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public enum ServerRequestStatus
{
	WAITING,
	SENT,
	COMPLETE
}
