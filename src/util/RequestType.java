package util;
import java.io.Serializable;

/**
 * This class specifies attributes that indicates which kind of request is received by a client.
 * <br>An enum is not used because it cannot be serialized (and thus, cannot be sent over a network).
 * 
 * @author Martin Graversen <martin@ownzone.org>
 * @version 1.0
 */
public class RequestType implements Serializable
{
	/** Indicates a connect request */
	public static final int CONNECT = 0;
	
	/** Indicates an intentional disconnect request */
	public static final int DISCONNECT = 1;
	
	/** Indicates a generic request, typically a task request */
	public static final int GENERIC = 2;
	
	private RequestType()
	{

	}
}
