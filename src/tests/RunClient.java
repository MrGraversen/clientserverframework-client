package tests;

import helpers.*;

import java.io.IOException;
import java.util.*;

import arduino.ArduinoLink;
import network.ServerRequest;
import util.*;
import client.*;
import exceptions.OwnzoneException;

public class RunClient
{
	public static void main(String[] args) throws InterruptedException
	{
		ConsoleHelper.enable();
		destructiveTest1();
//		ArduinoLink al = new ArduinoLink();
//		try
//		{
//			al.establishLink(9600, 2500);
//		}
//		catch (OwnzoneException e)
//		{
//			e.printStackTrace();
//		}
	}

	private static void destructiveTest3() throws InterruptedException
	{
		try
		{
			ClientConfiguration cc = new ClientConfiguration("localhost", 14000, "test4321", "MyClientTest");
			ClientConnection connection = ClientConnection.getClientInstance();
			connection.connect(cc);
			
			// Build and send the server request
			ServerRequest sreq = new ServerRequest("VerySlowTask");
			connection.sendServerRequest(sreq);
			
			// Shut down after small delay
			Thread.sleep(500);
			connection.disconnect();
		}
		catch (OwnzoneException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private static void destructiveTest2() throws InterruptedException
	{
		try
		{
			ClientConfiguration cc = new ClientConfiguration("localhost", 14000, "test4321", "MyClientTest");
			ClientConnection connection = ClientConnection.getClientInstance();
			connection.connect(cc);
			

			// Build and send the server request for a task that does not exist
			ServerRequest sreq = new ServerRequest("TaskThatDoesNotExist");
			UUID requestId = connection.sendServerRequest(sreq);

			// Wait for the server to respond
			ServerRequest response = connection.waitAndGetServerRequest(requestId);

			// Did the task fail?
			if (response.getResultNetworkPackage().failed())
			{
				// Print out the reason
				System.out.println("Request failed, reason: " + response.getResultNetworkPackage().getException().getMessage());
			}
			else
			{
				// We don't want to see this
				System.out.println("Request didn't fail.");
			}
		}
		catch (OwnzoneException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private static void destructiveTest1() throws InterruptedException
	{
		try
		{
			ClientConfiguration cc = new ClientConfiguration("localhost", 14000, "test4321", "MyClientTest");
			ClientConnection connection = ClientConnection.getClientInstance();
			connection.connect(cc);

			// Build and send the server request
			ServerRequest sreq = new ServerRequest("NullPointerTask");
			UUID requestId = connection.sendServerRequest(sreq);

			// Wait for the server to respond
			ServerRequest response = connection.waitAndGetServerRequest(requestId);

			// Did the task fail?
			if (response.getResultNetworkPackage().failed())
			{
				// Print out the reason
				System.out.println("Request failed, reason: " + response.getResultNetworkPackage().getException().getMessage());
			}
			else
			{
				// We don't want to see this
				System.out.println("Request didn't fail.");
			}
		}
		catch (OwnzoneException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private static void stressTest() throws InterruptedException
	{
		try
		{
			ClientConfiguration cc = new ClientConfiguration("localhost", 14000, "test4321", "MyClientTest");
			ClientConnection connection = ClientConnection.getClientInstance();
			connection.connect(cc);

			// Save the last request so that we can wait for it later
			UUID lastRequest = null;
			for (int i = 1; i <= 10000; i++)
			{
				// Generate random test data, 50 characters long
				String testData = RandomHelper.randomString(50);

				// Build and send the server request
				ServerRequest sreq = new ServerRequest("SecureStringTask");
				SecureString secureString = new SecureString(testData, CryptoHelper.getServerPublicKey());
				sreq.putProperty(new DynamicProperty<SecureString>("SecureString", "", secureString, false));
				sreq.putProperty(new DynamicProperty<String>("PlainText", "", testData, false));
				UUID tempId = connection.sendServerRequest(sreq);

				// Grab the last id
				if (i == 10000) lastRequest = tempId;

				// Wait 5 ms
				Thread.sleep(5);
			}

			// Wait for the last request to be completed
			connection.waitAndGetServerRequest(lastRequest);

			// Count and print the number of failed requests
			int failed = 0;
			ArrayList<ServerRequest> listOfRequests = connection.getAllServerRequests();
			for (ServerRequest request : listOfRequests)
			{
				if (request.getResultNetworkPackage().failed()) failed++;
			}
			System.out.println(listOfRequests.size() + " requests sent, failed: " + failed);
		}
		catch (OwnzoneException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
